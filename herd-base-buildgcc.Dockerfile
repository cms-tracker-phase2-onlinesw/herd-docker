FROM centos:7.9.2009 as base

LABEL author "Tom Williams <tom.williams@cern.ch>"

ARG TARGETPLATFORM


# Install common RPMs
RUN \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    echo "armhfp" > /etc/yum/vars/basearch; \
    echo "armv7hl" > /etc/yum/vars/arch; \
    echo "armv7hl-redhat-linux-gpu" > /etc/rpm/platform; \
    echo -e "[epel]\nname=Epel rebuild for armhfp\nbaseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/epel.repo; \
    mkdir -p /usr/local/lib64; \
  else \
    yum -y install epel-release; \
  fi && \
  yum -y install glibc libgcc \
    boost-filesystem boost-program-options boost-system boost-test boost-thread \
    jsoncpp log4cplus yaml-cpp && \
  yum clean all




FROM base as dev

RUN \
  yum -y install gcc-c++ make cmake3 gmp-devel mpfr-devel libmpc-devel && \
  yum clean all && \
  cd /tmp && \
  curl -LO ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-8.3.0/gcc-8.3.0.tar.gz && \
  tar xzf gcc-8.3.0.tar.gz && \
  mkdir /tmp/gcc-8.3.0-build && \
  cd /tmp/gcc-8.3.0-build && \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    ../gcc-8.3.0/configure --enable-languages=c,c++ --disable-multilib --with-tune=cortex-a8 --with-arch=armv7-a --with-float=hard --with-fpu=vfpv3-d16 --with-abi=aapcs-linux; \
  else \
    ../gcc-8.3.0/configure --enable-languages=c,c++ --disable-multilib; \
  fi && \
  time make -j4 && \
  time make install && \
  cd / && \
  rm -rf /tmp/gcc-8.3.0* && \
  yum -y remove gcc gcc-c++

RUN \
  yum -y install rpm-build git \
    boost-devel jsoncpp-devel log4cplus-devel yaml-cpp-devel && \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    # Patch for "No build ID note found in <PATH TO LIBRARY>" when creating debuginfo RPMs on armv7 CentOS7
    sed -ri 's/find-debuginfo.sh %\{._missing_build_ids_terminate_build:--strict-build-id\}/find-debuginfo.sh/' /usr/lib/rpm/macros; \
  fi && \
  yum clean all

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]




FROM base as app

COPY --from=dev /usr/local/lib/libgcc_s.so.1 /usr/local/lib/libstdc++.so.6.0.25 /usr/local/lib/

RUN \
  ln -s /usr/local/lib/libstdc++.so.6.0.25 /usr/local/lib/libstdc++.so.6

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]
