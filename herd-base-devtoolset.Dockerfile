FROM centos:7.9.2009 as base

LABEL author "Tom Williams <tom.williams@cern.ch>"

ARG TARGETPLATFORM


# Install common RPMs (required at runtime and for build)
RUN \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    echo "armhfp" > /etc/yum/vars/basearch; \
    echo "armv7hl" > /etc/yum/vars/arch; \
    echo "armv7hl-redhat-linux-gpu" > /etc/rpm/platform; \
    echo -e "[epel]\nname=Epel rebuild for armhfp\nbaseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/epel.repo; \
    mkdir -p /usr/local/lib64; \
  else \
    yum -y install epel-release centos-release-scl; \
  fi && \
  yum -y install glibc libgcc devtoolset-8-runtime \
    boost-filesystem boost-program-options boost-system boost-test boost-thread \
    jsoncpp log4cplus yaml-cpp && \
  yum clean all




FROM base as dev

RUN \
  yum -y install devtoolset-8-gcc-c++ make rpm-build git \
    boost-devel jsoncpp-devel log4cplus-devel yaml-cpp-devel && \
  # Install cmake3 from source if 3.17 not available (i.e. for arm64)
  if yum list available cmake3-3.17.*; then \
    yum -y install cmake3-3.17.*; \
  else \
    yum -y install dos2unix openssl-devel unzip && \
    cd /tmp && \
    curl -LO https://github.com/Kitware/CMake/releases/download/v3.17.5/cmake-3.17.5.zip && \
    unzip cmake-3.17.5.zip && \
    cd cmake-3.17.5 && \
    find . -type f -print0 | xargs -0 dos2unix || true && \
    scl enable devtoolset-8 'time ./bootstrap && time make -j$(nproc) && time make install' && \
    ln -s /usr/local/bin/cmake /usr/local/bin/cmake3; \
  fi && \
  yum clean all

# Default user & command
USER root
SHELL      ["/usr/bin/scl", "enable", "devtoolset-8", "--", "/bin/bash", "-c"]
ENTRYPOINT ["/usr/bin/scl", "enable", "devtoolset-8", "--", "/bin/bash", "-c"]
CMD ["bash"]




FROM base as app

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]
